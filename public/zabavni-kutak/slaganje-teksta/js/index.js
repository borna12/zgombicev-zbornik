$(function() {


    var scrollable = true;

    var listener = function(e) {
        if (! scrollable) {
            e.preventDefault();
        }
    }
    
    document.addEventListener('touchmove', listener, { passive:false });
    





    var drake = dragula([
        document.getElementById('drake-origin'),
    ], {
        accepts: function(el, target, source, sibling) {
            return target.id === "drake-origin";
        }
    }).on('drag', function(el, source) {
        scrollable = false;
        // On mobile this prevents the default page scrolling while dragging an item.
        
    }).on('drop', function(el, target, source, sibling) {
        scrollable = true;
        
    });


    var drake2 = dragula([
        document.getElementById('drake-origin2'),
    ], {
        accepts: function(el, target, source, sibling) {
            return target.id === "drake-origin2";
        }
    }).on('drag', function(el, source) {
        // On mobile this prevents the default page scrolling while dragging an item.
        scrollable = false;

    }).on('drop', function(el, target, source, sibling) {
        // On mobile this turns on default page scrolling after the end of a drag drop.
        scrollable = true;

    });
});
window.addEventListener( 'touchmove', function() {})
var bodovi = 0;
$(".init-page__btn2").hide()
function shuffle(array) { //izmješaj pitanja
    var i = 0,
        j = 0,
        temp = null

    for (i = array.length - 1; i > 0; i -= 1) {
        j = Math.floor(Math.random() * (i + 1))
        temp = array[i]
        array[i] = array[j]
        array[j] = temp
    }
}

function pitanja() {
    var lista = ["Marija Magdalena od Magdala grad-", "a vzivajet se . Rojena že bisi od č-", "astnih i v istinu ot roda kraljeva.", "Jeježe otac bê imenem Širus mati že njeje", "v istinu Eukarija imenujet se. Ponježe si boga-", "ta beše jere stvar bogatastva lice preo-", "brazujet i lepota njeje divna meju vsemi b-", "êše."]
    var lista2 = ["mari; magdalena od magdala grad", "a vzivaet se . roena že bisi od č", "astnih[ i v istinu ot roda kraleva", "ježe otac[ b; imenem[ širus[ mati že nee", "v istinu eukari; imenuet se . poneže si boga", "ta beše ere stvar[ bogatastva lice preo", "brazuet[ i lepota nee divna meû vsemi b", ";še ."]

    shuffle(lista)
    shuffle(lista2)
    lista.forEach(function(x) {
        $("#drake-origin").append('<li class="draggable" ><div>' + x + '</div></li>')
    })
    br = 0;
    lista2.forEach(function(x) {
        $("#drake-origin2").append('<li class="draggable" id="p' + br + '"><div>' + x + '</div></li>')
        br++
    })
}
pitanja();




$(".init-page__btn").on("click", function() {
    if ($("#drake-origin li").eq(0).html() == '<div>Marija Magdalena od Magdala grad-</div>') {
        $("#drake-origin li").eq(0).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin li").eq(0).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin li").eq(1).html() == '<div>a vzivajet se . Rojena že bisi od č-</div>') {
        $("#drake-origin li").eq(1).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin li").eq(1).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin li").eq(2).html() == '<div>astnih i v istinu ot roda kraljeva.</div>') {
        $("#drake-origin li").eq(2).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin li").eq(2).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin li").eq(3).html() == '<div>Jeježe otac bê imenem Širus mati že njeje</div>') {
        $("#drake-origin li").eq(3).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin li").eq(3).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin li").eq(4).html() == '<div>v istinu Eukarija imenujet se. Ponježe si boga-</div>') {
        $("#drake-origin li").eq(4).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin li").eq(4).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin li").eq(5).html() == '<div>ta beše jere stvar bogatastva lice preo-</div>') {
        $("#drake-origin li").eq(5).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin li").eq(5).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;

    }
    if ($("#drake-origin li").eq(6).html() == '<div>brazujet i lepota njeje divna meju vsemi b-</div>') {
        $("#drake-origin li").eq(6).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin li").eq(6).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin li").eq(7).html() == '<div>êše.</div>') {
        $("#drake-origin li").eq(7).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin li").eq(7).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    //glagoljski stupac
    if ($("#drake-origin2 li").eq(0).html() == '<div>mari; magdalena od magdala grad</div>') {
        $("#drake-origin2 li").eq(0).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(0).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(1).html() == '<div>a vzivaet se . roena že bisi od č</div>') {
        $("#drake-origin2 li").eq(1).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(1).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(2).html() == '<div>astnih[ i v istinu ot roda kraleva</div>') {
        $("#drake-origin2 li").eq(2).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(2).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(3).html() == '<div>ježe otac[ b; imenem[ širus[ mati že nee</div>') {
        $("#drake-origin2 li").eq(3).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(3).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(4).html() == '<div>v istinu eukari; imenuet se . poneže si boga</div>') {
        $("#drake-origin2 li").eq(4).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(4).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(5).html() == '<div>ta beše ere stvar[ bogatastva lice preo</div>') {
        $("#drake-origin2 li").eq(5).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(5).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(6).html() == '<div>brazuet[ i lepota nee divna meû vsemi b", ";še .</div>') {
        $("#drake-origin2 li").eq(6).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(6).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(7).html() == '<div>;še .</div>') {
        $("#drake-origin2 li").eq(7).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(7).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    $(".init-page__btn2").show(1200)
    $(".init-page__btn").hide(1200)
    $("#bodovi").html("Ostvareni broj bodova: <strong>" + bodovi + ".</strong>")
})