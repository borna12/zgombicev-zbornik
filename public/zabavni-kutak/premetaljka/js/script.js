var nalog = 0;
const scrabText = document.querySelectorAll(".scrab-text div");
const answerInput = document.querySelector(".answer input[type='text']");
const checkbtn = document.querySelector(".answer button");
const scoreBoard = document.querySelector(".score-board #score");
const resetBtn = document.querySelector(".score-board button");
const category = document.querySelectorAll("li[data-value]");
const catTag = document.querySelector(".category-tag h5");
var catValue = 1;
var words = new Array();
var hints = new Array();
var cat = "";
var correctAnswersCounter = 0;
var kajgod = 0;

checkbtn.addEventListener("click", checkWord);
resetBtn.addEventListener("click", resetScore);
answerInput.addEventListener("keypress", (e) => {
    e.keyCode == 13 ? checkWord() : "";
});

category.forEach(function(element) {
    element.addEventListener("click", changeCategory);
});

function changeCategory() {
    catValue = this.dataset.value;
    activeCard = 0;
    next = 0;
    setCategory();
    init();
};

function mjesaj(array, array2) { //izmješaj pitanja
    var i = 0,
        j = 0,
        temp = null,
        temp2 = null;
    var rndic = Math.floor(Math.random() * (i + 1))
    for (i = array.length - 1; i > 0; i -= 1) {
        j = Math.floor(Math.random() * (i + 1))
        temp = array[i]
        array[i] = array[j]
        array[j] = temp
        temp = array2[i]
        array2[i] = array2[j]
        array2[j] = temp
    }

}

//kategorije
function setCategory() {
    if (kajgod == 0) {
        let zivotinje = {
            words: ["glagoljica", "sunčev sustav", "malinska", "kurziv", "krk", "zadar", "istra", "ljekaruša", "azbuka"],
            hint: ["glagoljica.png", "suncev.jpg", "malinska.jpg", "kurziv.png", "krk.jpg", "zadar.jpg", "istra.jpg", "ljekarusa.jpg", "azbuka.jpg"],
            catName: "zivotinje"
        }

        let boje = {
            words: ["glagoljica", "staroslavenski institut", "malinska", "kurziv", "krk", "lucidar", "žgombićev zbornik", "ljekaruša", "azbuka"],
            hint: ["macka.jpg", "kokos.jpg", "majmun.png", "golub.png", "koza.png", "krava.png", "leptir.png", "pas.jpg", "patka.png", "zirafa.png", "mis.png"],
            catName: "zivotinje"
        }
        let categories = ["", boje, zivotinje];
        let choice = catValue;
        words = categories[choice].words;
        hints = categories[choice].hint;
        mjesaj(words, hints)
        cat = categories[choice].catName
        catTag.innerHTML = `${categories[choice].catName}  <i class="fa fa-caret-down"></i>`;
    }
}

var score = 0;
var next = 0;
var activeCard = 0;
var slide = 0;

function isteklo() {
    clearInterval(countdownTimer);
    swal({
        title: "Isteklo je vrijeme.",
        html: "<p style='text-align:center'><strong>Točan odgovor je <span style='color:#bb422a; font-size:34px' >" + words[next] + "</span></strong>.</p><br><em>",
        showCloseButton: true,
        confirmButtonText: ' dalje',
        backdrop: false,
        allowOutsideClick: false,
        allowEscapeKey: false
    });
    $(".swal2-confirm").on("click", function() {
        switchCards();
        next++;
        init();
    })
    $(".swal2-close").on("click", function() {
        switchCards();
        next++;
        init();
    })
    correctAnswersCounter -= 1
}

function ProgressCountdown(timeleft, bar, text) {
    if (window.countdownTimer) {
        clearInterval(countdownTimer);
    }
    return new Promise((resolve, reject) => {
        countdownTimer = setInterval(() => {
            timeleft--;
            document.getElementById(bar).value = timeleft;
            document.getElementById(text).textContent = timeleft;
            if (timeleft <= 0) {
                clearInterval(countdownTimer);
                resolve(true);
            } else if (timeleft <= 1) {
                $("#sekunde").html("sekunda")
                $("#ostalo").html("ostala")
            } else if (timeleft <= 4) {
                $("#sekunde").html("sekunde")
            }
        }, 1000);
    });
}
init();
/*
 * Initializes the random Texts on the cards
 */
clearInterval(countdownTimer);

function init() {
    $("#unos").val("");
    setCategory();
    $("#unos").focus()
    if (next < words.length) {
        if (activeCard == 1) {
            scrabText[activeCard].innerText = shuffle(words[next]);
            $(".slika").attr("src", "slike/" + cat + "/" + hints[next])
            $(".slika").delay(800).fadeIn();
            activeCard = 0;
        } else {
            $(".slika").attr("src", "slike/" + cat + "/" + hints[next])
            $(".slika").fadeIn();
            scrabText[activeCard].innerText = shuffle(words[next]);
            activeCard = 1;
            ProgressCountdown(120, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => this.isteklo());
        }

    }
}
$("#unos").keyup(function() {
    $("#tekstic2").text($("#unos").val())
});

$(".game-bg").hide()
swal({
    title: "Premetaljka",
    html: '<p>Rasporedi prikazana glagoljska slova tako da dobiješ ispravnu riječ. Slova upiši pomoću tipkovnice.</p> <button class="btn">igraj</button>',
    showCloseButton: false,
    showConfirmButton: false,
    confirmButtonText: ' dalje',
    backdrop: false,
    allowOutsideClick: false,
    allowEscapeKey: false,
    allowEnterKey: false
});

$(".btn").on("click", function() {
    swal.close()
    $(".game-bg").fadeIn(900)
    $(".ziv").click()
    kajgod = 1;
})

/*
 * Checks if the player's answer Matches the Correct Word.
 *and increases the score if it does.
 */
function checkWord(e) {
    let answer = answerInput.value.toLowerCase();
    let correct = (answer == words[next]);
    if (correct === true) {
        answerInput.value = "";
        $(".slika").fadeOut();
        score += parseInt($("#pageBeginCountdownText").text());
        setScore();
        //turns the background of the active card to green if answer is correct
        scrabText[Math.abs(activeCard - 1)].style.background = "#C5E1A5";
        correctAnswersCounter++;
        clearInterval(countdownTimer);
        setTimeout(function() {
            if (next < words.length) {
                switchCards();
            } else {

                swal({
                    title: "Hvala Vam na sudjelovanju u kvizu!<br>",
                    html: '<p>S obzirom na količinu točnih odgovora i vrijeme ostvraili ste idući broj bodova:</p><form class="jedan" action="https://docs.google.com/forms/d/e/1FAIpQLSctoZLhqWPgA1b4sKbvW78IxVIdwyHRVmBxbqh3g5TuQ9t9HQ/formResponse" target="_self" id="bootstrapForm" method="POST"> <legend for="736982347"> <select id="ikona"> </select> Ime: <input id="312289462" type="text" name="entry.312289462" class="form-control" required id="input-q1" style="height:30px; text-align: center;"> </legend> <div class="form-group" style="display:none"> <input id="60656686" type="text" name="entry.60656686" class="form-control" required> </div><div class="form-group" style="display:none"> <input id="1487903547" type="text" name="entry.1487903547" class="form-control" required> </div><input type="hidden" name="fvv" value="1"> <input type="hidden" name="fbzx" value="-2538308583737878367"> <input type="hidden" name="pageHistory" value="0"> <button class="btn" type="submit" id="predaj"> predaj</button></form><button class="btn" id="reset" onclick="resetScore()">nova igra</button>',
                    showCloseButton: false,
                    showConfirmButton: false,
                    confirmButtonText: ' dalje',
                    backdrop: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false
                });

                $("#60656686").attr("value", score)
                postotak = Math.floor((correctAnswersCounter / words.length) * 100)
                if (postotak >= 90) {
                    ocjena = 5
                } else if (postotak >= 80) {
                    ocjena = 4
                } else if (postotak >= 70) {
                    ocjena = 3
                } else if (postotak >= 60) {
                    ocjena = 2
                } else {
                    ocjena = 1
                }
                $("#1487903547").attr("value", ocjena)
                if (cat == "zivotinje") { $('#bootstrapForm').attr('action', 'https://docs.google.com/forms/d/e/1FAIpQLSctoZLhqWPgA1b4sKbvW78IxVIdwyHRVmBxbqh3g5TuQ9t9HQ/formResponse'); } else if (cat == "boje") { $('#bootstrapForm').attr('action', ' https://docs.google.com/forms/d/e/1FAIpQLSctoZLhqWPgA1b4sKbvW78IxVIdwyHRVmBxbqh3g5TuQ9t9HQ/formResponse'); }
                nalog = 1;
                switchCards();
                $(".results-page__info").text(score)
                $(".scrab-text").html("")

                var target = document.getElementById("ikona");
                var emojiCount = emoji.length;
                for (var index = 0; index < emojiCount; index++) {
                    addEmoji(emoji[index]);
                }

                function addEmoji(code) {
                    var option = document.createElement('option');
                    option.innerHTML = code;
                    option.value = code;
                    target.appendChild(option);
                }
                if (localStorage.getItem("ime") != null) {
                    $('#312289462').val(localStorage.getItem("ime"))
                    $('#ikona').val(localStorage.getItem("ikona"))
                }
                $('#bootstrapForm').submit(function(event) {
                    localStorage.setItem("ime", $('#312289462').val())
                    localStorage.setItem("ikona", $('#ikona').val())
                    localStorage.setItem('pokrenuto', "da")
                    event.preventDefault()
                    $('#312289462').val(
                        document.getElementById("ikona").value + document.getElementById("312289462").value
                    )
                    $("#predaj").hide(300)
                    event.preventDefault()
                    var extraData = {}
                    $('#bootstrapForm').ajaxSubmit({
                        data: extraData,
                        dataType: 'jsonp', // This won't really work. It's just to use a GET instead of a POST to allow cookies from different domain.
                        error: function() {
                            // Submit of form should be successful but JSONP callback will fail because Google Forms
                            // does not support it, so this is handled as a failure.
                            window.open("rez.html", "_self");
                        }
                    })
                })
            }
        }, 800);
        next++;
        init();
    } else {
        //turns the background of the active card to red if answer is incorrect
        scrabText[Math.abs(activeCard - 1)].style.background = "#EF9A9A";
        score -= 5
        correctAnswersCounter -= 1;
        setScore();
    }
    e.preventDefault();
}


function switchCards() {
    $("#tekstic2").text("")
    slide++;
    if (nalog == 0) {
        ProgressCountdown(120, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => this.isteklo());
    }
    for (let i = 0; i < scrabText.length; i++) {
        scrabText[i].style.background = "#fff";
        if (scrabText[i].classList.contains("isActive")) {
            scrabText[i].classList.add("notActive");
            scrabText[i].classList.remove("isActive");
            slide = 0;
        } else {
            if (scrabText[i].classList.contains("notActive")) {
                scrabText[i].classList.remove("notActive");
            }
            scrabText[i].classList.add("isActive");
        }
    }
}

function setScore() {
    scoreBoard.innerText = score;
}

function resetScore() {

    window.location.reload();
}
/*
 *This function randomizes the letters in the Word.
 *passed to it.
 */
/*Using Fisher-Yates Shuffle Algorithm*/
function shuffle(word) {
    let wordChar = word.split("");
    for (i = 0; i < wordChar.length; i++) {
        let rand = Math.ceil(Math.random() * word.length - 1);
        temp = wordChar[i];
        wordChar[i] = wordChar[rand];
        wordChar[rand] = temp;
    }
    //Turns shuffled Array into a String and returns it.
    return wordChar.join("");
}