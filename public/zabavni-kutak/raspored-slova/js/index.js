$(function() {
    var scrollable = true;

    var listener = function(e) {
        if (!scrollable) {
            e.preventDefault();
        }
    }

    document.addEventListener('touchmove', listener, { passive: false });
    var drake2 = dragula([
        document.getElementById('drake-origin2'),
    ], {
        accepts: function(el, target, source, sibling) {
            return target.id === "drake-origin2";
        }
    }).on('drag', function(el, source) {
        // On mobile this prevents the default page scrolling while dragging an item.
        scrollable = false;

    }).on('drop', function(el, target, source, sibling) {
        // On mobile this turns on default page scrolling after the end of a drag drop.
        scrollable = true;

    });
});
window.addEventListener('touchmove', function() {})
var bodovi = 0;

function shuffle(array) { //izmješaj pitanja
    var i = 0,
        j = 0,
        temp = null

    for (i = array.length - 1; i > 0; i -= 1) {
        j = Math.floor(Math.random() * (i + 1))
        temp = array[i]
        array[i] = array[j]
        array[j] = temp
    }
}
$(".init-page__btn2").hide()
function pitanja() {
    var lista2 = [". a. ", ". b. ", ". v. ", ". g. ", ". d. ", ". e. ", ". ž. ", ". Z. ", ". z. ", ". I. "]

    shuffle(lista2)

    br = 0;
    lista2.forEach(function(x) {
        $("#drake-origin2").append('<li class="draggable" id="p' + br + '"><div>' + x + '</div></li>')
        br++
    })
}
pitanja();




$(".init-page__btn").on("click", function() {
    if ($("#drake-origin2 li").eq(0).html() == '<div>. a. </div>') {
        $("#drake-origin2 li").eq(0).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(0).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(1).html() == '<div>. b. </div>') {
        $("#drake-origin2 li").eq(1).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(1).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(2).html() == '<div>. v. </div>') {
        $("#drake-origin2 li").eq(2).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(2).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(3).html() == '<div>. g. </div>') {
        $("#drake-origin2 li").eq(3).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(3).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(4).html() == '<div>. d. </div>') {
        $("#drake-origin2 li").eq(4).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(4).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(5).html() == '<div>. e. </div>') {
        $("#drake-origin2 li").eq(5).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(5).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(6).html() == '<div>. ž. </div>') {
        $("#drake-origin2 li").eq(6).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(6).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(7).html() == '<div>. Z. </div>') {
        $("#drake-origin2 li").eq(7).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(7).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(8).html() == '<div>. z. </div>') {
        $("#drake-origin2 li").eq(8).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(8).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }
    if ($("#drake-origin2 li").eq(9).html() == '<div>. I. </div>') {
        $("#drake-origin2 li").eq(9).css({
            "background": "#81C784",
        })
        bodovi++;
    } else {
        $("#drake-origin2 li").eq(9).css({
            "background": "#EF9A9A"
        })
        bodovi -= 1;
    }

    $(".init-page__btn2").show(1200)

    $(".init-page__btn").hide(1200)
    $("#bodovi").html("Ostvareni broj bodova: <strong>" + bodovi + ".</strong>")
})